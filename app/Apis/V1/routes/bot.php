<?php

use App\Apis\V1\Bot\Http\Controllers\BotsController;

Route::prefix('/api/v1')->group(
    fn() => Route::controller(BotsController::class)
        ->group(function () {
            Route::get('/', 'index');
        }),
);
