<?php

namespace App\Apis\V1\Bot\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BotRepository.
 *
 * @package namespace App\Repositories;
 */
interface BotRepository extends RepositoryInterface
{
    //
}
