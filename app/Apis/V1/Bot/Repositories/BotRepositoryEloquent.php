<?php

namespace App\Apis\V1\Bot\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Apis\V1\Bot\Repositories\BotRepository;
use App\Models\Bot;
use App\Apis\V1\Bot\Validators\BotValidator;

/**
 * Class BotRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BotRepositoryEloquent extends BaseRepository implements BotRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Bot::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
