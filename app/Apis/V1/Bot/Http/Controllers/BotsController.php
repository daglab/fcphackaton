<?php

namespace App\Apis\V1\Bot\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use App\Apis\V1\Bot\Repositories\BotRepository;
use App\Apis\V1\Bot\Validators\BotValidator;


use App\Apis\V1\Bot\Http\Requests\{
    BotCreateRequest,
    BotUpdateRequest,
};

use DefStudio\Telegraph\Models\TelegraphChat;
use Illuminate\Support\Facades\DB;


use DefStudio\Telegraph\Keyboard\Button;
use DefStudio\Telegraph\Keyboard\Keyboard;


use App\Services\DatabaseHealthCheck;
use App\Services\StorageHealthCheck;
use App\Services\HttpHealthCheck;
use App\Services\FtpHealthCheck;

use Illuminate\Database\Connection;
use Exception;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Http;
/**
 * Class BotsController.
 *
 * @package namespace App\Http\Controllers;
 */
class BotsController extends Controller
{
    /**
     * @var BotRepository
     */
    protected $repository;

    /**
     * @var BotValidator
     */
    protected $validator;

    /**
     * BotsController constructor.
     *
     * @param BotRepository $repository
     * @param BotValidator $validator
     */
    public function __construct(BotRepository $repository, BotValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        // $prompt = 'тест';
        // $response = Http::withoutVerifying()
        //     ->withHeaders([
        //         'Authorization' => 'Bearer ' . env('CHATGPT_API_KEY'),
        //         'Content-Type' => 'application/json',
        //     ])->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
        //         "prompt" => $prompt,
        //         "max_tokens" => 1000,
        //         "temperature" => 0.5
        //     ]);

       // return $response->json()['choices'][0]['text'];

        // $this->reply("Грузим...");
        // $this->chat->message($response->json()['choices'][0]['text'])->send();


        dd($response->json()['choices'][0]['text']);

        // $query = DB::raw("select current_timestamp-query_start as runtime,datname,usename, query FROM pg_stat_activity where state='active' order by 1 desc limit 1;");
        // $string = $query->getValue(DB::connection()->getQueryGrammar());
        // $status = DB::select($string);

        // dd($status);



        // $result = Process::run("dir/s");
        // echo $result->output();

        $chats = TelegraphChat::get();


        $resLongQuery = DB::raw("SELECT now() - query_start as runtime,pid, usename, datname, wait_event, state, query FROM pg_stat_activity WHERE now() - query_start > '12 seconds'::interval and state='active' ORDER BY runtime DESC");
        $longqueryString = $resLongQuery->getValue(DB::connection()->getQueryGrammar());
        $statuses = DB::select($longqueryString);
        dd($statuses);

        if (!empty($statuses)) {
            foreach ($statuses as $status) {
                foreach ($chats as $chat) {

                    $chat->message('Cложный запрос: ' . PHP_EOL . $status->runtime . PHP_EOL . $status->usename . PHP_EOL . $status->datname . PHP_EOL . $status->wait_event . PHP_EOL . $status->state . PHP_EOL . $status->query)
                        ->keyboard(Keyboard::make()->buttons([
                            Button::make("✅ Исправить")->action("repair")->param('pid', $status->pid),

                            Button::make("📖 Помощь AI")->action("read")->param('id', '3'),
                        ])->chunk(2))->send();
                }
            }
        }

dd('stop');

        //IdleQuery

        //   SELECT pid , query, * from pg_stat_activity
        //   WHERE state != 'idle' ORDER BY xact_start;


//         $resIdleQuery = DB::raw("SELECT pid,query,state, xact_start, now() - xact_start AS duration FROM pg_stat_activity WHERE state LIKE '%transaction%' ORDER BY 3 DESC");
//         $idlequeryString = $resIdleQuery->getValue(DB::connection()->getQueryGrammar());
//         $statuses = DB::select($idlequeryString);



//         if (!empty($statuses)) {
//             foreach ($statuses as $status) {
//                 foreach ($chats as $chat) {

//                     $chat->message('Зависшая транзакция: ' . PHP_EOL . $status->pid . PHP_EOL . $status->query . PHP_EOL . $status->xact_start . PHP_EOL . $status->duration . PHP_EOL . $status->state . PHP_EOL . $status->query)
//                         ->keyboard(Keyboard::make()->buttons([
//                             Button::make("✅ Исправить")->action("repair")->param('pid', $status->pid),
//                             Button::make("📖 Помощь AI")->action("helpai")->param('id', '3'),
//                         ])->chunk(2))->send();
//                 }
//             }
//         }

// dd($statuses);

        // dd('stop');
        // $bots = $this->repository->all();

        // Database Health connection  check
        $db = new DatabaseManager();
      //  $db->addConnection('default', DB::connection());
         $db->addConnection('default', new BadConnection);

        $status = (new DatabaseHealthCheck($db))->status();

        if ($status->status !== 'OK') {
            foreach ($chats as $chat) {

                $chat->message($status->name . ' ' . $status->status . ' ' . $status->message)
                    ->keyboard(Keyboard::make()->buttons([
                        Button::make("✅ Исправить")->action("repair")->param('pid', '1'),

                        Button::make("📖 Помощь AI")->action("read")->param('id', '3'),
                    ])->chunk(2))->send();
            }
        }

        dd('stop');

        // Storeage Health read,write,delete check
        $status = (new StorageHealthCheck())->status();

        if ($status->status !== 'OK') {
            foreach ($chats as $chat) {
                $chat->message($status->name . ' ' . $status->status . ' ' . $status->message)
                    ->keyboard(Keyboard::make()->buttons([
                        Button::make("✅ Исправить")->action("delete")->param('id', '1'),

                        Button::make("📖 Помощь AI")->action("read")->param('id', '3'),
                    ])->chunk(2))->send();
            }
        }


        // Http Health read,write,delete check
        $status = (new HttpHealthCheck())->status();

        if ($status->status !== 'OK') {
            foreach ($chats as $chat) {
                $chat->message($status->name . ' ' . $status->status . ' ' . $status->message)
                    ->keyboard(Keyboard::make()->buttons([
                        Button::make("✅ Исправить")->action("delete")->param('id', '1'),

                        Button::make("📖 Помощь AI")->action("read")->param('id', '3'),
                    ])->chunk(2))->send();
            }
        }


        // // Http Health read,write,delete check
        // $status = (new FtpHealthCheck())->status();

        // if ($status->status !== 'OK') {
        //     foreach ($chats as $chat) {
        //         $chat->message($status->name . ' ' . $status->status . ' ' . $status->message)
        //             ->keyboard(Keyboard::make()->buttons([
        //                 Button::make("✅ Исправить")->action("delete")->param('id', '1'),
        //                 Button::make("📖 Пометить прочитанным")->action("read")->param('id', '2'),
        //                 Button::make("📖 Помощь AI")->action("read")->param('id', '3'),
        //             ])->chunk(2))->send();
        //     }
        // }


        // dd($status);

        $activity = DB::table('pg_stat_activity')->get();

        // $chat->message($activity[0]->backend_start)->send();
        // dd($activity);

        //Поиск сложных запросов
        // SELECT now () - query_start as "runtime", usename, datname, wait_event, state, query FROM pg_stat_activity WHERE now () - query_start> '10 seconds' :: interval and state = 'active' ORDER BY runtime DESC;

        //"idle" status - остановка неактивных
        // SELECT pg_cancel_backend (procpid);
        // SELECT pg_terminate_backend (procpid);

        //незавершенные транзакции
        // SELECT pid, xact_start, now () - xact_start AS duration FROM pg_stat_activity WHERE state LIKE '% transaction%' ORDER BY 3 DESC;

        //cron запуск

        // очередь отправки в редис






        return 'ok';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BotCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(BotCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $bot = $this->repository->create($request->all());

            $response = [
                'message' => 'Bot created.',
                'data'    => $bot->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bot = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $bot,
            ]);
        }

        return view('bots.show', compact('bot'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bot = $this->repository->find($id);

        return view('bots.edit', compact('bot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BotUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(BotUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $bot = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Bot updated.',
                'data'    => $bot->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Bot deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Bot deleted.');
    }
}


class HealthyConnection extends Connection
{
    public function __construct()
    {
    }

    public function getPdo()
    {
        return true;
    }
}

class BadConnection extends Connection
{
    public function __construct()
    {
    }

    public function getPdo()
    {
        throw new Exception;
    }
}

class DatabaseManager extends \Illuminate\Database\DatabaseManager
{
    protected $connections = [];

    public function __construct()
    {
    }

    public function connection($name = null)
    {
        if (!$name) {
            return $this->connection('default');
        }

        if (!isset($this->connections[$name])) {
            throw new \InvalidArgumentException("Database [$name] not configured.");
        }

        return $this->connections[$name];
    }

    public function addConnection($name, $connection)
    {
        $this->connections[$name] = $connection;
    }
}
