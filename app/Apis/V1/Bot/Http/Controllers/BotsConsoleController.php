<?php

namespace App\Apis\V1\Bot\Http\Controllers;

use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\DB;
use DefStudio\Telegraph\Models\TelegraphChat;
use DefStudio\Telegraph\Keyboard\Button;
use DefStudio\Telegraph\Keyboard\Keyboard;

use App\Services\DatabaseHealthCheck;
use App\Services\StorageHealthCheck;
use App\Services\HttpHealthCheck;

use Illuminate\Database\Connection;
use Exception;

/**
 * Class BotsConsoleController.
 *
 * @package namespace App\Http\Controllers;
 */
class BotsConsoleController extends Controller
{

    public function index()
    {


        $chats = TelegraphChat::get();


        // Database Health connection  check
        $db = new DatabaseManager();
      // $db->addConnection('default', DB::connection());
       $db->addConnection('default', new BadConnection);

        $status = (new DatabaseHealthCheck($db))->status();



        if ($status->status !== 'OK') {
            foreach ($chats as $chat) {

                $chat->message($status->name . PHP_EOL . $status->status . PHP_EOL . $status->message . PHP_EOL .'Трасcировка:' . PHP_EOL .json_encode($status->context))
                    ->keyboard(Keyboard::make()->buttons([
                        Button::make("✅ Перезапустить")->action("rebootpg")->param('reboot', '1'),
                        Button::make("📖 Помощь AI")->action("asktochatgpt")->param('prompt', $status->message),
                    ])->chunk(4))->send();
            }
        }



        // Storeage Health read,write,delete check
        $status = (new StorageHealthCheck())->status();

        if ($status->status !== 'OK') {
            foreach ($chats as $chat) {
                $chat->message($status->name . ' ' . $status->status . ' ' . $status->message)
                    ->keyboard(Keyboard::make()->buttons([
                       // Button::make("✅ Исправить")->action("clear")->param('id', '1'),

                        Button::make("📖 Помощь AI")->action("asktochatgpt")->param('prompt', $status->message),
                    ])->chunk(2))->send();
            }
        }


        // Http Health read,write,delete check
        $status = (new HttpHealthCheck())->status();

        if ($status->status !== 'OK') {
            foreach ($chats as $chat) {
                $chat->message($status->name . ' ' . $status->status . ' ' . $status->message)
                    ->keyboard(Keyboard::make()->buttons([
                     //   Button::make("✅ Исправить")->action("delete")->param('id', '1'),

                        Button::make("📖 Помощь AI")->action("asktochatgpt")->param('prompt', $status->message),
                    ])->chunk(2))->send();
            }
        }


        //Complex query:
        $resLongQuery = DB::raw("SELECT now() - query_start as runtime,pid, usename, datname, wait_event, state, query FROM pg_stat_activity WHERE now() - query_start > '12 seconds'::interval and state='active' ORDER BY runtime DESC");
        $longqueryString = $resLongQuery->getValue(DB::connection()->getQueryGrammar());
        $statuses = DB::select($longqueryString);


        if (!empty($statuses)) {
            foreach ($statuses as $status) {
                foreach ($chats as $chat) {

                    $chat->message('Cложный запрос: ' . PHP_EOL . $status->pid . PHP_EOL . $status->runtime . PHP_EOL . $status->usename . PHP_EOL . $status->datname . PHP_EOL . $status->wait_event . PHP_EOL . $status->state . PHP_EOL . $status->query)
                        ->keyboard(Keyboard::make()->buttons([
                            Button::make("✅ Исправить")->action("repair")->param('pid', $status->pid),

                            Button::make("📖 Помощь AI")->action("asktochatgpt")->param('prompt', $status->usename . PHP_EOL . $status->datname . PHP_EOL . $status->wait_event . PHP_EOL . $status->state . PHP_EOL . $status->query),
                        ])->chunk(2))->send();
                }
            }
        }



        // IdleQuery
        $resIdleQuery = DB::raw("SELECT pid,query,state, xact_start, now() - xact_start AS duration FROM pg_stat_activity WHERE state LIKE '%transaction%' ORDER BY 3 DESC");
        $idlequeryString = $resIdleQuery->getValue(DB::connection()->getQueryGrammar());
        $statuses = DB::select($idlequeryString);



        if (!empty($statuses)) {
            foreach ($statuses as $status) {
                foreach ($chats as $chat) {

                    $chat->message('Зависшая транзакция: ' . PHP_EOL . $status->pid . PHP_EOL . $status->query . PHP_EOL . $status->xact_start . PHP_EOL . $status->duration . PHP_EOL . $status->state . PHP_EOL . $status->query)
                        ->keyboard(Keyboard::make()->buttons([
                            Button::make("✅ Исправить")->action("repair")->param('pid', $status->pid),
                            Button::make("📖 Помощь AI")->action("asktochatgpt")->param('prompt', '3'),
                        ])->chunk(2))->send();
                }
            }
        }

        // dd($statuses);
        // dd('stop');


        return 'ok';
    }
}


class HealthyConnection extends Connection
{
    public function __construct()
    {
    }

    public function getPdo()
    {
        return true;
    }
}

class BadConnection extends Connection
{
    public function __construct()
    {
    }

    public function getPdo()
    {
        throw new Exception;
    }
}

class DatabaseManager extends \Illuminate\Database\DatabaseManager
{
    protected $connections = [];

    public function __construct()
    {
    }

    public function connection($name = null)
    {
        if (!$name) {
            return $this->connection('default');
        }

        if (!isset($this->connections[$name])) {
            throw new \InvalidArgumentException("Database [$name] not configured.");
        }

        return $this->connections[$name];
    }

    public function addConnection($name, $connection)
    {
        $this->connections[$name] = $connection;
    }
}
