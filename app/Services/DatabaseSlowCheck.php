<?php

namespace App\Services;

use Exception;
use Illuminate\Database\DatabaseManager;
use App\Services\HealthCheck;
use Illuminate\Support\Facades\DB;

class DatabaseHealthCheck extends HealthCheck
{
    protected $name = 'database';


    public function status()
    {
       
            try {

                $resIdleQuery = DB::raw("SELECT pid,query,state, xact_start, now() - xact_start AS duration FROM pg_stat_activity WHERE state LIKE '%transaction%' ORDER BY 3 DESC");
                $idlequeryString = $resIdleQuery->getValue(DB::connection()->getQueryGrammar());
                $statuses = DB::select($idlequeryString);





            } catch (Exception $e) {
                return $this->problem('Could not connect to db', [
                    'connection' => $connection,
                    'exception' => $this->exceptionContext($e),
                ]);
            }


        return $this->okay();
    }
}
