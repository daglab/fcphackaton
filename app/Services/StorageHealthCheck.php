<?php

namespace App\Services;


use Illuminate\Support\Facades\Storage;
use Exception;
use App\Services\HealthCheck;

class StorageHealthCheck extends HealthCheck
{
    protected $name = 'storage';

    protected $workingDisks = [];

    protected $corruptedFiles = [];

    protected $exceptions = [];

    public function status()
    {
        $uniqueString = uniqid('hack-health-check_', true);

        foreach (config('healthcheck.storage.disks') as $disk) {
            try {
                $storage = Storage::disk($disk);

                $storage->put($uniqueString, $uniqueString);

                $contents = $storage->get($uniqueString);

                $storage->delete($uniqueString);

                if ($contents !== $uniqueString) {
                    $this->corruptedFiles[] = [
                        'disk' => $disk,
                        'incorrect_contents' => $contents,
                    ];

                    continue;
                }

                $this->workingDisks[] = $disk;
            } catch (Exception $e) {
                $this->exceptions[] = [
                    'disk' => $disk,
                    'error' => $this->exceptionContext($e),
                ];
            }
        }

        if (empty($this->corruptedFiles) && empty($this->exceptions)) {
            return $this->okay();
        }

        return $this->problem(
            'Some storage disks are not working',
            [
                'working' => $this->workingDisks,
                'corrupted_files' => $this->corruptedFiles,
                'exceptions' => $this->exceptions,
            ]
        );
    }
}
