<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use DirectoryIterator;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });


        $this->mapModulesRoutes();
        $this->mapApiV1Routes();
    }

    protected function mapApiV1Routes()
    {
        $dir = new DirectoryIterator(app_path('Apis/V1/routes'));
        foreach ($dir as $file) {
            if ($file->isFile() && !in_array($file->getFilename(), ['api.php', 'web.php', 'console.php', 'channels.php'])) {
                Route::middleware('api')
                    ->group(app_path('Apis/V1/routes/' . $file->getFilename()));
            }
        }
    }

    protected function mapModulesRoutes()
    {
        $dir = new DirectoryIterator(base_path('routes'));
        foreach ($dir as $file) {
            if ($file->isFile() && !in_array($file->getFilename(), ['api.php', 'web.php', 'console.php', 'channels.php'])) {
                Route::middleware('web')
                    ->group(base_path('routes/' . $file->getFilename()));
            }
        }
    }

}
