<?php

namespace App\Providers;

use App\Apis\V1\Bot\Repositories\BotRepository;
use App\Apis\V1\Bot\Repositories\BotRepositoryEloquent;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->app->bind(BotRepository::class, BotRepositoryEloquent::class);
        //:end-bindings:
    }
}
