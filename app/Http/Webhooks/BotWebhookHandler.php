<?php

namespace App\Http\Webhooks;

use DefStudio\Telegraph\Handlers\WebhookHandler;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\Services\HealthCheck;
use Exception;

use Illuminate\Support\Facades\Process;

class BotWebhookHandler extends WebhookHandler
{
    public function start(): void
    {


        $query = DB::raw("select current_timestamp-query_start as runtime,datname,usename, query FROM pg_stat_activity where state='active' order by 1 desc limit 1;");
        $string = $query->getValue(DB::connection()->getQueryGrammar());
        $status = DB::select($string);


        $query2 = DB::raw("SELECT pid, wait_event_type, wait_event FROM pg_stat_activity WHERE wait_event is NOT NULL and wait_event_type='LWLock'");
        $string = $query2->getValue(DB::connection()->getQueryGrammar());
        $status2 = DB::select($string);




        $this->chat->message("Добрый день!" . PHP_EOL . "продолжительность самой долгой транзакции:" . $status[0]->runtime . PHP_EOL . "Запрос: " . $status[0]->query . PHP_EOL . "")->send();

        foreach ($status2 as $item) {
            $this->chat->message("События ожидания LWlock:" . PHP_EOL . "PID:" . $item->pid . PHP_EOL . "Тип ожидания: " . $item->wait_event_type . PHP_EOL . "Событие: " . $item->wait_event . "")->send();
        }

        
    }

    public function repair()
    {
        $repairpid = $this->data->get('pid');


        $query = DB::raw("SELECT pg_terminate_backend(" . $repairpid . ")");
        $longqueryString = $query->getValue(DB::connection()->getQueryGrammar());
        $status = DB::select($longqueryString);

        $this->reply("Запрос " . $repairpid . " завершен");
        $this->chat->message("Запрос " . $repairpid . " завершен")->send();
    }

    public function rebootpg()
    {

        //$this->chat->message('Тест')->send();

        try {
            $reboot = $this->data->get('reboot');

            if ($reboot == 1) {
                $query = DB::raw("CHECKPOINT");
                $string = $query->getValue(DB::connection()->getQueryGrammar());
                $status = DB::select($string);

                $result = Process::run('pg_ctl -D "C:\OSPanel\userdata\PostgreSQL-10" restart');
            }
            $this->reply("Перезагрузка...");
            $this->chat->message("Перезгрузка завершена удачно")->send();
        } catch (Exception $e) {
            $this->chat->message('Лог ошибки перезагрузки:' . json_encode(
                [
                    'error' => $e->getMessage(),
                    'class' => get_class($e),
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'trace' => explode("\n", $e->getTraceAsString()),
                ]
            ))->send();
        }
    }

    public function stoppg()
    {

        try {
            $stop = $this->data->get('stoppg');

            if ($stop == 1) {

                $result = Process::run('pg_ctl -D "C:\OSPanel\userdata\PostgreSQL-10" stop');
            }
            $this->reply("Остановка...");
            $this->chat->message("Остановка завершена удачно")->send();
        } catch (Exception $e) {
            $this->chat->message('Лог ошибки остановки:' . json_encode(
                [
                    'error' => $e->getMessage(),
                    'class' => get_class($e),
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'trace' => explode("\n", $e->getTraceAsString()),
                ]
            ))->send();
        }
    }

    public function startpg()
    {

        try {
            $start = $this->data->get('startpg');

            if ($start == 1) {

                $result = Process::run('pg_ctl -D "C:\OSPanel\userdata\PostgreSQL-10" start');
            }
            $this->reply("Запуск...");
            $this->chat->message("Перезагрузка завершена удачно")->send();
        } catch (Exception $e) {
            $this->chat->message('Лог ошибки перезагрузки: ' . json_encode(
                [
                    'error' => $e->getMessage(),
                    'class' => get_class($e),
                    'line' => $e->getLine(),
                    'file' => $e->getFile(),
                    'trace' => explode("\n", $e->getTraceAsString()),
                ]
            ))->send();
        }
    }


    public function asktochatgpt()
    {
        $prompt = $this->data->get('prompt');
        $response = Http::withoutVerifying()
            ->withHeaders([
                'Authorization' => 'Bearer ' . env('CHATGPT_API_KEY'),
                'Content-Type' => 'application/json',
            ])->post('https://api.openai.com/v1/engines/text-davinci-003/completions', [
                "prompt" => 'Ты лучший системный администратор серверов Postgres, подскажи что делать, на сервере такая ошибка:' . $prompt,
                "max_tokens" => 2000,
                "temperature" => 0.5
            ]);

        // return $response->json()['choices'][0]['text'];

        $this->reply("Грузим...");
        $this->chat->message($response->json()['choices'][0]['text'])->send();
    }
}
