<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BotRepository;
use App\Entities\Bot;
use App\Validators\BotValidator;

/**
 * Class BotRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BotRepositoryEloquent extends BaseRepository implements BotRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Bot::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
