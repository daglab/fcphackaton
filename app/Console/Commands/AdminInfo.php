<?php

namespace App\Console\Commands;

use App\Apis\V1\Bot\Http\Controllers\BotsConsoleController;
use Illuminate\Console\Command;

class AdminInfo extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка системы и информирование админа';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    //cd public_html && php artisan admin:info >> /dev/null 2>&1

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void {
		(new BotsConsoleController())->index();
    }
}
