<?php

return [
    /*
     * Base path for the health check endpoints, by default will use /
     */
    'base-path' => '',

    /**
     * Paths to host the health check and ping endpoints
     */
    'route-paths' => [
        'health' => '/health',
        'ping' => '/ping',
    ],

    /*
     * List of health checks to run when determining the health
     * of the service
     */
    'checks' => [
        App\Services\DatabaseHealthCheck::class,
    ],



    /*
     * Used by the basic auth middleware
     */
    'auth' => [
        'user' => env('HEALTH_CHECK_USER'),
        'password' => env('HEALTH_CHECK_PASSWORD'),
    ],

    /*
     * Routename for the healthcheck
     */
    'route-name' => 'healthcheck',

    /*
     * Can define a list of connection names to test. Names can be
     * found in your config/database.php file. By default, we just
     * check the 'default' connection
     */
    'database' => [
        'connections' => ['default'],
    ],

    /*
     * Can give an array of required environment values, for example
     * 'REDIS_HOST'. If any don't exist, then it'll be surfaced in the
     * context of the healthcheck
     */
    'required-env' => [],

    /*
     * List of addresses and expected response codes to
     * monitor when running the HTTP health check
     *
     * e.g. address => response code
     */
    'addresses' => [],

    /*
     * Default response code for HTTP health check. Will be used
     * when one isn't provided in the addresses config.
     */
    'default-response-code' => 200,

    /*
     * Default timeout for cURL requests for HTTP health check.
     */
    'default-curl-timeout' => 2.0,


    /*
     * A list of disks to be checked by the Storage health check
     */
    'storage' => [
        'disks' => [
            'local',
        ],
    ],

    /*
     * A list of packages to be ignored by the Package Security health check
     */
    'package-security' => [
        'ignore' => [],
    ],

    'scheduler' => [
        'cache-key' => 'laravel-scheduler-health-check',
        'minutes-between-checks' => 5,
    ],

];
